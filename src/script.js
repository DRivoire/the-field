import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js'

import grassVertexShader from './shaders/vertex.glsl'
import grassFragmentShader from './shaders/fragment.glsl'

/**
 * Base
 */
// Debug
const gui = new dat.GUI()
const debugObject = {}
debugObject.grassBottomColor = '#000000'
debugObject.grassTopColor = '#10be00'
debugObject.floorColor = '#c54040'
gui.addColor(debugObject, 'grassTopColor').onChange(() => { grassShader.uniforms.uGrassTopColor.value.set(debugObject.grassTopColor) })
gui.addColor(debugObject, 'grassBottomColor').onChange(() => { grassShader.uniforms.uGrassBottomColor.value.set(debugObject.grassBottomColor) })

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()
scene.background = new THREE.Color('#335577')

/**
 * Models
 */
const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('/draco/')

const gltfLoader = new GLTFLoader()
gltfLoader.setDRACOLoader(dracoLoader)


let grassArrayPositions = []

const grassGeo = new THREE.BufferGeometry()
// const grassMat = new THREE.MeshBasicMaterial

const textureLoader = new THREE.TextureLoader()
const grassTexture = textureLoader.load('/images/grass-texture.jpg')

const grassShader = new THREE.ShaderMaterial({
    vertexShader: grassVertexShader,
    fragmentShader: grassFragmentShader,
    vertexColors: true,
    transparent: true,
    alphaTest: false,
    // lights: true,
    uniforms:
    {
        uSize: { value: 58.0 },
        uTime: { value: 0 },
        uGrassTopColor: { value: new THREE.Color('#10be00')},
        uGrassBottomColor: { value: new THREE.Color('black')},
        uTexture: { value: grassTexture }
    }
})
gui.add(grassShader.uniforms.uSize, 'value').name('Taille herbe').min(0).max(150).step(0.01)


gltfLoader.load(
    '/models/Field/the-field-ultra.gltf',
    (gltf) =>
    {
        const children = [...gltf.scene.children]

        gltf.scene.traverse( function( child ) {
            if ( child.isMesh ) {
                child.castShadow = true;
                child.receiveShadow = true;
            }
        })

        gltf.scene.rotation.y = Math.PI/2

        const floor = children[2]

        let vertices = floor.geometry.attributes.color.array

        for ( let i = 0; i < vertices.length; i++ ) {
            grassArrayPositions.push(floor.geometry.attributes.position.array[i]*18)
        }

        // console.log(grassArrayPositions)
        const positions = new Float32Array(grassArrayPositions)

        grassGeo.setAttribute('position', new THREE.BufferAttribute(positions, 3))

        const grass = new THREE.Points(grassGeo, grassShader)
        grass.rotation.y = Math.PI/2

        scene.add(grass)
        scene.add(gltf.scene)
    }
)
/**
 * Lights
 */
const ambientLight = new THREE.AmbientLight(0xffffff, 1)
scene.add(ambientLight)

const directionalLight = new THREE.DirectionalLight(0xffffff, 0.6)
directionalLight.castShadow = true
directionalLight.shadow.mapSize.set(1024, 1024)
directionalLight.shadow.camera.far = 15
directionalLight.shadow.camera.left = - 7
directionalLight.shadow.camera.top = 7
directionalLight.shadow.camera.right = 7
directionalLight.shadow.camera.bottom = - 7
directionalLight.position.set(5, 5, 5)
directionalLight.shadow.normalBias = 0.03
scene.add(directionalLight)

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.set(2, 2, 2)
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.target.set(0, 0.75, 0)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.shadowMap.enabled = true
renderer.shadowMap.type = THREE.PCFSoftShadowMap
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */
const clock = new THREE.Clock()
let previousTime = 0

const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()
    const deltaTime = elapsedTime - previousTime
    previousTime = elapsedTime

    grassShader.uniforms.uTime = elapsedTime

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()