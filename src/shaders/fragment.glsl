uniform vec3 uGrassTopColor;
uniform vec3 uGrassBottomColor;
uniform sampler2D uTexture;

varying vec2 vUv;

void main()
{

    vec4 textureColor = texture2D(uTexture, gl_PointCoord);

    vec3 verticalGradient = mix( uGrassTopColor,uGrassBottomColor, gl_PointCoord.y );

    float opacity = step(0.5, textureColor.x);
    // float opacity = textureColor;
    gl_FragColor = vec4(verticalGradient, opacity);
}