uniform float uTime;
uniform float uSize;

varying vec2 vUv;

void main()
{
    vec3 newPos = vec3(position.x, position.y + 0.01, position.z );
    vec4 modelPosition =  modelMatrix * vec4(newPos, 1.0);

    vec4 viewPosition = viewMatrix * modelPosition;
    vec4 projectedPosition = projectionMatrix * viewPosition;

    gl_Position = projectedPosition;
    gl_PointSize = uSize * 12.0;
    gl_PointSize *= ( 1.0 / - viewPosition.z );

    vUv = uv;
}